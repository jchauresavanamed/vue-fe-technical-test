# Savana front-end tech test

Hi there! Welcome to Savana's front-end technical test.

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Run your unit tests

```
yarn test:unit
```
